This is the currency ticker project.

Make sure 'npm install' is run before anything else.

**RUN A DEV BUILD**

A dev build can be created by running:

npm start

**RUN A PROD BUILD**

A production build can be created by running:

npm run build

**RUN TESTS**

Tests can be run by:

npm test

**CUSTOMLY ADD CURRENCIES**

If more currencies wish to be priced, the ticker can be added to the fxpairs array in App.js
 