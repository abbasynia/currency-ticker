import React from 'react';
import App from '../main/App';
import Enzyme, {mount} from "enzyme";
import expect from "must";
import Adapter from 'enzyme-adapter-react-16';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import {api_key} from "../config";


Enzyme.configure({ adapter: new Adapter() });

const mountComponent = (fxPairs) => (
    mount(<App fxPairs={fxPairs}/>)
);

beforeEach(() => {
    jest.useFakeTimers();
});

afterEach(() => {
    jest.runAllTimers();
});

it('returns prices of currency pairs on click', () => {
    const fxPairs = ["EURUSD", "USDCHF", "USDJPY"];
    const responseData = [
        {ticker: "EURUSD", price: "0.0123"},
        {ticker: "USDCHF", price: "1.234"},
        {ticker: "USDJPY", price: "0.98883"}
    ];

    let mock = new MockAdapter(axios);
    mock.onGet(`http://forex.1forge.com/1.0.3/quotes?pairs=${fxPairs.join()}&api_key=${api_key}`)
        .reply(200, responseData);

    const component = mountComponent(fxPairs);
    expect(component.find(".t-price").at(0).text()).to.eql("...");
    expect(component.find(".t-price").at(1).text()).to.eql("...");
    expect(component.find(".t-price").at(2).text()).to.eql("...");

    component.find(".t-update-prices").simulate("click");

    setImmediate(() => {
        expect(component.find(".t-price").at(0).text()).to.eql("0.0123");
        expect(component.find(".t-price").at(1).text()).to.eql("1.234");
        expect(component.find(".t-price").at(2).text()).to.eql("0.98883");
    });
});

it('returns prices of additional currency pairs customly added to component', () => {
    const fxPairs = ["EURUSD", "USDCHF", "USDJPY", "GBPUSD"];
    const responseData = [
        {ticker: "EURUSD", price: "0.0123"},
        {ticker: "USDCHF", price: "1.234"},
        {ticker: "USDJPY", price: "0.98883"},
        {ticker: "GBPUSD", price: "1.0123"}
    ];

    let mock = new MockAdapter(axios);
    mock.onGet(`http://forex.1forge.com/1.0.3/quotes?pairs=${fxPairs.join()}&api_key=${api_key}`)
        .reply(200, responseData);

    const component = mountComponent(fxPairs);
    expect(component.find(".t-price").at(0).text()).to.eql("...");
    expect(component.find(".t-price").at(1).text()).to.eql("...");
    expect(component.find(".t-price").at(2).text()).to.eql("...");
    expect(component.find(".t-price").at(3).text()).to.eql("...");

    component.find(".t-update-prices").simulate("click");

    setImmediate(() => {
        expect(component.find(".t-price").at(0).text()).to.eql("0.0123");
        expect(component.find(".t-price").at(1).text()).to.eql("1.234");
        expect(component.find(".t-price").at(2).text()).to.eql("0.98883");
        expect(component.find(".t-price").at(3).text()).to.eql("1.0123");
    });
});

it('returns error when api gives inconsistent response', () => {
    const fxPairs = ["EURUSD", "USDCHF", "USDJPY", "GBPUSD"];
    const responseData = [
        {ticker: "EURUSD", price: "0.0123"},
        {ticker: "USDCHF", price: "1.234"},
        {ticker: "USDJPY", price: "0.98883"},
    ];

    let mock = new MockAdapter(axios);
    mock.onGet(`http://forex.1forge.com/1.0.3/quotes?pairs=${fxPairs.join()}&api_key=${api_key}`)
        .reply(200, responseData);

    const component = mountComponent(fxPairs);
    expect(component.find(".t-price").at(0).text()).to.eql("...");
    expect(component.find(".t-price").at(1).text()).to.eql("...");
    expect(component.find(".t-price").at(2).text()).to.eql("...");
    expect(component.find(".t-price").at(3).text()).to.eql("...");

    component.find(".t-update-prices").simulate("click");

    setImmediate(() => {
        expect(component.find(".t-price").at(0).text()).to.eql("Error retrieving price.");
        expect(component.find(".t-price").at(1).text()).to.eql("Error retrieving price.");
        expect(component.find(".t-price").at(2).text()).to.eql("Error retrieving price.");
        expect(component.find(".t-price").at(3).text()).to.eql("Error retrieving price.");
    });
});

it('returns error when api gives malformed response', () => {
    const fxPairs = ["EURUSD", "USDCHF", "USDJPY"];
    const responseData = [
        {ticker: "EURUSD", price: "0.0123"},
        {ticker: "USDCHF", price: "1.234"},
        {ticker: "USDJPY"}
    ];

    let mock = new MockAdapter(axios);
    mock.onGet(`http://forex.1forge.com/1.0.3/quotes?pairs=${fxPairs.join()}&api_key=${api_key}`)
        .reply(200, responseData);

    const component = mountComponent(fxPairs);
    expect(component.find(".t-price").at(0).text()).to.eql("...");
    expect(component.find(".t-price").at(1).text()).to.eql("...");
    expect(component.find(".t-price").at(2).text()).to.eql("...");

    component.find(".t-update-prices").simulate("click");

    setImmediate(() => {
        expect(component.find(".t-price").at(0).text()).to.eql("0.0123");
        expect(component.find(".t-price").at(1).text()).to.eql("1.234");
        expect(component.find(".t-price").at(2).text()).to.eql("Error retrieving price.");
    });
});
