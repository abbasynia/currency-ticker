import React from 'react';
import styled from 'styled-components';

export const SymbolTicker = ({symbol, price}) => (
    <SymbolTickerContainer>
        <Symbol>{symbol}: </Symbol>
        <div className="t-price">{price}</div>
    </SymbolTickerContainer>
);

const SymbolTickerContainer = styled.div`
  display: flex;
  padding: 10px 0px;
`;

const Symbol = styled.div`
  margin-right: 30px;
`;