import React, { Component } from 'react';
import axios from 'axios';
import _ from "lodash";
import {SymbolTicker} from "./components/StockTicker";
import styled from 'styled-components';
import {api_key} from "../config";


const fxPairs = [
    "EURUSD",
    "USDCHF",
    "USDJPY",
];

class App extends Component {

    constructor(props) {
        super(props);
        const pairsToBePriced = props.fxPairs || fxPairs;
        this.state = pairsToBePriced.reduce((acc, cur) =>
            {
                acc[cur] = "...";
                return acc;
            }, {pairsToBePriced: pairsToBePriced});
        this._updatePrices = this.updatePrices.bind(this);
    }

    render() {
        return (
          <Container>
              {
                  this.state.pairsToBePriced.map((pair) =>
                      <SymbolTicker key={pair} symbol={pair} price={this.state[pair]}/>
                  )
              }
              <button className="t-update-prices" onClick={this._updatePrices}>Get latest Price.</button>
          </Container>
        );
    }

    updatePrices () {
        axios.get(`http://forex.1forge.com/1.0.3/quotes?pairs=${this.state.pairsToBePriced.join()}&api_key=${api_key}`)
            .then(res => {
                const pairsToBePriced = this.state.pairsToBePriced;
                if (res.data.length === pairsToBePriced.length) {
                        this.setState(_.zipObject(pairsToBePriced, res.data.map(p => p.price || "Error retrieving price.")));
                    } else {
                        this.setState(_.zipObject(pairsToBePriced, new Array(pairsToBePriced.length).fill("Error retrieving price.")));
                    }
                }
            )
    }
}

export default App;

const Container = styled.div`
  margin: 30px;
`;